var MINLICENSTEML = 3;

var epmtyLicense = {"localId":0,"ip":"", "os":"", "bits":0, "period":0, "installation":"", "installationfare":0.0, "rentfare":0.0} 
var licenseList = [];
var lastId = 0;
var itemsInCart = 2;
var valueInCart = 815;


//system

function scrollToElement(elId){
  $("body").animate({
        scrollTop: $(elId).offset().top - 50
    }, 500);
}

function getLicenseByLocalId(licenseLocalId){
    for (var i = licenseList.length - 1; i >= 0; i--){
        if(licenseList[i].localId == licenseLocalId){
            return licenseList[i];
        }
    };    
}

function initLicenses(){
    for (var i = 1; i <= MINLICENSTEML; i++) {
        licenseList.push($.extend({}, epmtyLicense));
        lastId = i
        licenseList[i-1].localId = lastId        
    };
}



//render

function appendLicense(appendAutomatically){
    if(appendAutomatically!=true){appendAutomatically=false} // undefined or object
    licenseList.push($.extend({}, epmtyLicense));
    licenseList[licenseList.length-1].localId = ++lastId        

    updateLicensesList(!appendAutomatically);
}
function updateLicensesList(ainmated){
    $("div#licensesList").empty();
    $("#licenseItemTmpl").tmpl(licenseList).appendTo("div#licensesList");
    if((ainmated!=undefined) && ainmated){
        $("div#licensePanel"+lastId).hide().fadeIn( "slow" );
        scrollToElement("div#licensePanel"+lastId);
    }

    $("button.addToCart").click(addToCart);
    $("span.IPToolTip").tooltip({"placement":"right"});

    $("input.IPAssressInput").keyup(IPAssressChanging)
    $("input.IPAssressInput").change(IPAssressWasChanged)
    $("input.IPAssressInput").focusout(IPAssressWasChanged)
    $("select.OSSelector").change(OSWasChanged)
    $("select.archSelector").change(archChanged)
    $("select.periodSelector").change(periodWasChanged)
    $("input.installationBySpecialist").change(specialistWasChanged)

}



function updateCartInfo(ainmated){
    $("span#itemsInCart").text(itemsInCart);
    $("span#valieInCart").text("$"+valueInCart);
    if((ainmated!=undefined) && ainmated){
        if(!$("body").hasClass("pinToBrowser")){
            $("body, div.topBar").addClass("pinToBrowser");    
            $("div.topBar").removeClass("invertColor").addClass("invertColor",300);
        }
        if(licenseList.length<MINLICENSTEML){
            // TODO: TBD
            appendLicense(true);
        }
    }
}
function addToCart(){

    var licenseLocalId = parseInt(this.id.replace("addToCart",""));

    $("select#archSelector"+licenseLocalId).trigger("change");
    $("select#periodSelector"+licenseLocalId).trigger("change");
    $("input#installationBySpecialist"+licenseLocalId).trigger("change");

    $("div#licensePanel"+licenseLocalId).slideUp( "slow" , function(){



        console.log(getLicenseByLocalId(licenseLocalId));
        itemWasAddedInToCart(licenseLocalId);

    });        
    //TODO: call itemWasAddedInToCart after server response
    //$.ajax({ url: "", dataType: "json", cache: false, success: function( data ) {    itemWasAddedInToCart(licenseLocalId);     }, error: function(){} });
    
}
function itemWasAddedInToCart(licenseLocalId){
    for (var i = licenseList.length - 1; i >= 0; i--){
        if(licenseList[i].localId == licenseLocalId){
            itemsInCart++;
            valueInCart += ( licenseList[i].installationfare + licenseList[i].rentfare );

            licenseList.splice(i, 1);
        }
    };    
    updateCartInfo(true);
}




function recalculateFare(id){
    $("span#installationFare"+id).text(getLicenseByLocalId(id).installationfare)
    $("span#rentFare"+id).text(getLicenseByLocalId(id).rentfare)
    $("span#fare"+id).text(getLicenseByLocalId(id).installationfare + getLicenseByLocalId(id).rentfare)
}


// validation 

function IPValidation(licenseLocalId,finalChaeck){
    var valid = true;
    val = $("input#IPAssressInput"+licenseLocalId).val().split(".")   
    if((!finalChaeck && val.length > 4) || (finalChaeck && val.length != 4)){
        valid = false;
    }
    for (var i = val.length - 1; i >= 0; i--) {
        var digit = parseInt(val[i])
        if( !( ( !finalChaeck && val[i] == "" && i==val.length-1) || ( val[i] == digit+"" && (digit>=0 && digit<=255) ) ) ) {
            valid = false;
        }
    };
    if (!valid){
        $("input#IPAssressInput"+licenseLocalId).removeClass("validFild").addClass("invalidFild");
        $("span#invalidIP"+licenseLocalId).show()
        return false;
    }else{
        $("input#IPAssressInput"+licenseLocalId).removeClass("invalidFild").addClass("validFild");        
        $("span#invalidIP"+licenseLocalId).hide()
        return true;
    }

}
function IPAssressChanging(){
    IPValidation(parseInt(this.id.replace("IPAssressInput","")),false)
}
function IPAssressWasChanged(){    
    var licenseLocalId = parseInt(this.id.replace("IPAssressInput",""));
    if(IPValidation(licenseLocalId,true)){
        getLicenseByLocalId(licenseLocalId).ip = $("input#"+this.id).val()
    }
    tryToEnableOrderBtn(licenseLocalId)
}
function OSWasChanged(){
    var licenseLocalId = parseInt(this.id.replace("OSSelector",""));
    var osId = $("select#"+this.id).val()
    $("select#archSelector"+licenseLocalId).prop("disabled", (osId != "")?false:"disabled");
    getLicenseByLocalId(licenseLocalId).os = osId
    tryToEnableOrderBtn(licenseLocalId)
}

function archChanged(){
    var licenseLocalId = parseInt(this.id.replace("archSelector",""));    
    getLicenseByLocalId(licenseLocalId).bits = $("option:selected",this).val()
}

function periodWasChanged(){
    var licenseLocalId = parseInt(this.id.replace("periodSelector",""));
    var obj = getLicenseByLocalId(licenseLocalId)
    obj.period = $("option:selected",this).val()
    obj.rentfare = parseFloat($("option:selected" ,this).attr("price"))

    recalculateFare(licenseLocalId)
    
}
function specialistWasChanged(){    
    var licenseLocalId = parseInt(this.id.replace("installationBySpecialist",""));

    $("select#periodSelector"+licenseLocalId).trigger("change");
    var obj = getLicenseByLocalId(licenseLocalId)
    obj.installation = $("input#"+this.id).prop("checked")
    obj.installationfare = (obj.installation)?parseFloat($("input#"+this.id).attr("price")):0;

    recalculateFare(licenseLocalId)
}


function tryToEnableOrderBtn(id){
    $("button#addToCart"+id).prop("disabled", (getLicenseByLocalId(id).ip != "" && getLicenseByLocalId(id).os != "")?false:"disabled");
    
}








$(document).ready(function() {    
    $("div#cartLinkSpot").tooltip({"placement":"bottom"});

    initLicenses();
    updateLicensesList();
    updateCartInfo();
    $("div#addNewLicense").click(appendLicense);    
});
